class Paddle {
    constructor(posX, posY, up, down) {
        this.posX = posX
        this.posY = posY
        this.width = 5
        this.length = 50
        this.up = up
        this.down = down
        document.addEventListener('keydown', (event) => {
            if (event.keyCode === this.up) {
                this.moveUp()
            }
            if (event.keyCode === this.down) {
                this.moveDown()
            }
        });
    }

    render(ctx) {
        ctx.fillStyle = 'white';
        ctx.fillRect(this.posX, this.posY, this.width, this.length);
    }

    moveUp() {
        this.posY -= 15;
    }

    moveDown() {
        this.posY += 15;
    }

}