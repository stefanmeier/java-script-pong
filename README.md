## JavaScript Pong
  
This code is only for people who want to see my very early code.
  
A replica of the pong game using Canvas (HTML object for rendering in 2D with JavaScript).  
This is an exercise from the first week at Propulsion Academy.  
  
The game is not finished, as ther is no collision detection and the paddles go faster the more you use them.  
To take a look just put the HTML and the JS files in the same Directory and open the HTML with a browser.
The Paddles are controlled with arrow up, arrow down, W and S.  