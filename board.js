class Board {
    constructor(ctx) {
        this.ctx = ctx;
    }

    newBoard() {
        this.ctx.fillStyle = 'red';
        this.ctx.fillRect(0, 0, 500, 500);
    }
}