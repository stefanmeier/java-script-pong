class Ball {
    constructor(posX, posY, velX, velY) {
        this.posX = posX
        this.posY = posY
        this.velX = velX
        this.velY = velY
    }

    render(ctx) {
        ctx.beginPath();
        ctx.arc(this.posX, this.posY, 10, 0, 2 * Math.PI, false);
        ctx.fillStyle = 'yellow';
        ctx.fill();
    }

    moveBall() {
        this.posX += this.velX
        this.posY += this.velY
        if (this.posX + 10 === 500 || this.posX - 10 === 0) {
            // this.velX = -this.velX;
            this.velX = alert("haha");
        }
        if (this.posY + 10 === 500 || this.posY - 10 === 0) {
            // this.velY = -this.velY;
            this.velY = -this.velY;
        }
    }
}