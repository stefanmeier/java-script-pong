class Game {
    constructor() {
        this.canvas = document.querySelector('#canvas');
        this.ctx = canvas.getContext('2d');
        this.board = new Board(this.ctx)
        this.ball = new Ball(20, 20, 5, 5)
        this.paddle1 = new Paddle(5, 300, 87, 83)
        this.paddle2 = new Paddle(490, 300, 38, 40)
    }

    play() {
        setInterval(() => {
            this.board.newBoard()
            this.ball.render(this.ctx)
            this.ball.moveBall()
            this.paddle1.render(this.ctx)
            this.paddle2.render(this.ctx)
        }, 16);
    }

}